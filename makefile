#
#  MAKEFILE
#
#    Steven Hale
#    2019 June 8
#    Birmingham, UK
#
# This is a Jekyll website.  To build it you need
#   # dnf install ruby ruby-devel @development-tools
#

################################################################

.PHONY: all dep build serve clean

all: build

################################################################

##
# Tell the user what the targets are.
##

help:
	@echo
	@echo "Targets:"
	@echo "   dep      Install dependencies."
	@echo "   build    Build the static site."
	@echo "   serve    Serve the site from localhost:4000."
	@echo "   draft    Serve the site from localhost:4000 including _drafts."
	@echo "   clean    Remove all files and directories in .gitignore."
	@echo

# Run in --silent mode unless the user sets VERBOSE=1 on the
# command-line.

ifndef VERBOSE
.SILENT:
endif

dep:
	bundle config set --local path 'vendor/bundle'
	bundle install

build: dep
	JEKYLL_ENV=production \
	bundle exec jekyll build

serve: dep
	JEKYLL_ENV=production \
	bundle exec jekyll serve

draft: dep
	JEKYLL_ENV=production \
	bundle exec jekyll serve --draft

clean:
	git clean -dxf
