---
title: Operations
permalink: /background/operations
key: background-operations
---

## The Tools We Need To Do Our Research

In order to investigate solar oscillations the group conceived, built
and currently operates a global network of observatories dedicated to
the continuous observation of the Sun.

Each station is equipped with an array of sophisticated
instruments. The main devices that are used by the group are
resonant-scattering spectrometers. These devices work in a different
manner to the more familiar grating spectrometer. The
resonant-scattering component within our spectrometers consists of a
small silica vapour cell, containing an ensemble of potassium atoms
heated to ~100 <sup>o</sup>C, so that they form a vapour. The cell is
held in a strong magnetic field that moves the working parts of the
vapour into the blue and red wings of the Solar Fraunhofer Line. The
assembly on either side of the cell focuses resonantly-scattered light
onto two solid state detectors. This arrangement is shown
schematically below:

![Splitting]({{site.baseurl}}/assets/images/pages/whatwe1.gif "Splitting")

Photodetectors or photomultiplier tubes are used in the BiSON
instruments to measure the intensity of light scattered from a small,
glass cell containing potassium vapour. These devices output a voltage
proportional to the intensity of the light falling on their active
surfaces. The output voltage is passed through a voltage-to-frequency
(V/F) converter and the resulting pulses are routed to a set of
counters where they can be subsequently processed.

The polarization of the incoming light is controlled by special
electronics. The same hardware also sends the appropriate gate signals
to the counters. For each sample the counter gates are held open for
3.2 seconds. The gates are then closed for 0.8 seconds to allow the
readout electronics to read the accumulated counts from the
counters. In this way, one line-of-sight measurement of the Sun is
taken every four seconds.
       
## Data Transfer and Preprocessing

In order to save CPU time, the four-second data collected throughout
the day is written directly to a raw data file.  At sunset some
compacting software reads this file, performs some statistical
analysis on the data and then writes out a daily file that contains
data in a forty-second format. The daily data is transferred from the
remote stations to Birmingham either across a modem link or via the
internet.
