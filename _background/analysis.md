---
title: Data Analysis
permalink: /background/analysis
key: background-analysis
---

## Science From Our Data

Once back in Birmingham the data are processed to extract velocity
information from the surface of the Sun. The plot below shows what a
typical day's data looks like from one of our stations. The x-axis
gives the time in Universal Time and the y-axis represents the
measured intensity of the light received by the detectors in one of
our spectrometers. As the day progresses the intensity steadily
increases as the sun rises, flattens off around midday and falls away
towards sunset. This particular day was from our Sutherland station
and appears to have had a fairly clear morning and a cloudy afternoon
with sharp drop in intensity at about 15.5 Hours UT.

![how1]({{site.baseurl}}/assets/images/pages/howwed1.gif "how1")

We are primarily interested in extracting the Doppler information from
the solar surface. To do this we need a way of converting from the
intensity information above into a velocity measurement. We take the
intensity as measured at the blue and red wings of the solar line,
I<sub>B</sub> and I<sub>R</sub> respectively, and we then construct
the following relation:

<p>R = (I<sub>B</sub> - I<sub>R</sub>) / (I<sub>B</sub> + I<sub>R</sub>)</p>

The (I<sub>B</sub> + I<sub>R</sub>) on the bottom acts to normalise
the data so that variations in one detector will be reflected in the
other. I we produce a plot of R we get something which looks like the
following:

![how2]({{site.baseurl}}/assets/images/pages/howwed2.gif "how2")

We then multiply the ratio by a factor which has the effect of
converting the above plot into one with units of velocity.  Finally a
curve is fitted to the measured velocity ratio and then subtracted off
it. What remains are our velocity residuals.
