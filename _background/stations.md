---
title: Stations
permalink: /background/stations
key: background-stations
---

## Mount Wilson, California, USA

This instrument was installed in 1992, and uses a small fraction of
the beam from the [60-foot solar tower](http://physics.usc.edu/solar)
(shown above). It was initially installed on Haleakala in Hawaii
(1981), where it operated until 1991. This picture was taken from the
[150-foot solar tower](http://www.astro.ucla.edu/~obs/intro.html).

<a href="{{site.baseurl}}/assets/images/pages/mt_wilson_big.gif"><img
style="float: left" alt="[Mount Wilson site picture]"
src="{{site.baseurl}}/assets/images/pages/mt_wilson_big.gif" width="280" height="436"></a>

<iframe width="280" height="436" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.uk/maps?f=q&amp;iwloc=&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=34.2240,+-118.05855&amp;aq=&amp;sll=34.224001,-118.058599&amp;sspn=0.000894,0.001742&amp;ie=UTF8&amp;t=h&amp;ll=34.224835,-118.057709&amp;spn=0.003868,0.003004&amp;z=17&amp;output=embed"></iframe>

## Las Campanas, Chile

Las Campanas was commissioned in 1991.  It is equatorially mounted and
housed in a small dome.  This site consistently produces the best
BiSON data.

<a href="{{site.baseurl}}/assets/images/pages/CP-26.jpg"><img
style="float: left" alt="[Las Campanas site picture]"
src="{{site.baseurl}}/assets/images/pages/CP-26-small.jpg"></a>

<iframe width="280" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.uk/maps?f=q&amp;iwloc=&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=-29.02168,+-70.68983&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=55.411532,114.169922&amp;ie=UTF8&amp;t=h&amp;ll=-29.020244,-70.690498&amp;spn=0.005254,0.005987&amp;z=16&amp;output=embed"></iframe>

<a href="{{site.baseurl}}/assets/images/pages/CP-20.jpg"><img
style="float: none" alt="[Las Campanas site picture]"
src="{{site.baseurl}}/assets/images/pages/CP-20-small.jpg"></a>

<a href="{{site.baseurl}}/assets/images/pages/CO-26.jpg"><img
style="float: none" alt="[Las Campanas site picture]"
src="{{site.baseurl}}/assets/images/pages/CO-26-small.jpg"></a>

<a href="{{site.baseurl}}/assets/images/pages/FD-10.jpg"><img
style="float: none" alt="[Las Campanas site picture]"
src="{{site.baseurl}}/assets/images/pages/FD-10-small.jpg"></a>

<a href="{{site.baseurl}}/assets/images/pages/FD-18.jpg"><img
style="float: none" alt="[Las Campanas site picture]"
src="{{site.baseurl}}/assets/images/pages/FD-18-small.jpg"></a>

<a href="{{site.baseurl}}/assets/images/pages/lco_fox_big.gif"><img
style="float: none" alt="[Las Campanas site picture]"
src="{{site.baseurl}}/assets/images/pages/lco_fox.gif"></a>

## Sutherland, South Africa

The instrument is installed in a dome situated at the [South African
Astronomical Observatory](http://www.saao.ac.za) (SAAO), at
Sutherland, Cape Province. It was commissioned in, and has been
producing data since 1990.

<p><a href="{{site.baseurl}}/assets/images/pages/BY-26.jpg"><img
style="float: left" alt="[Sutherland site picture]"
src="{{site.baseurl}}/assets/images/pages/BY-26-small.jpg"></a></p>

<iframe width="280" height="282" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.uk/maps?f=q&amp;iwloc=&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=-32.38044,+20.81055&amp;aq=&amp;sll=-32.380451,20.810444&amp;sspn=0.001225,0.002411&amp;ie=UTF8&amp;t=h&amp;ll=-32.377714,20.80965&amp;spn=0.010148,0.011973&amp;z=15&amp;output=embed"></iframe>

<p><a href="{{site.baseurl}}/assets/images/pages/AV-9.jpg"><img
style="float: none" alt="[Sutherland site picture]"
src="{{site.baseurl}}/assets/images/pages/AV-9-small.jpg"></a></p>

<p><a href="{{site.baseurl}}/assets/images/pages/AV-14.jpg"><img
style="float: none" alt="[Sutherland site picture]"
src="{{site.baseurl}}/assets/images/pages/AV-14-small.jpg"></a></p>

<p><a href="{{site.baseurl}}/assets/images/pages/saao_springbok_big.gif"><img
style="float: none" alt="[Sutherland site picture]"
src="{{site.baseurl}}/assets/images/pages/saao_springbok.gif"></a></p>

## Izaña, Tenerife

Iza&ntilde;a is the longest-running site of BiSON and has been
producing data since 1975. Our station is situated at the
[Observatorio del Teide](http://www.iac.es/eno.php?op1=3&lang=en) (in
the pyramid-like building). It is operated under a collaboration with
the I.A.C., and requires daily attention.

Unlike the more recent stations, this spectrometer is not equatorially
mounted. Sunlight is fed into the instrument using a coelostat (shown
in the above image) - an arrangement of mirrors which change their
orientations during the day to follow the course of the Sun. The
output beam is then reflected through a hole in the wall of the
pyramid into the instrument.

<a href="{{site.baseurl}}/assets/images/pages/izana_coelostat_big.gif"><img
style="float: left" alt="[Izana site picture]"
src="{{site.baseurl}}/assets/images/pages/izana_coelostat.gif"></a>

<iframe width="280" height="444" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.uk/maps?f=q&amp;iwloc=&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=28.300158,+-16.512065&amp;aq=&amp;sll=-32.380433,20.810552&amp;sspn=0.001225,0.002411&amp;ie=UTF8&amp;t=h&amp;ll=28.300876,-16.511089&amp;spn=0.004194,0.003004&amp;z=17&amp;output=embed"></iframe>

## Carnarvon, Western Australia

Carnarvon was the third BiSON site and was the first to have a
spectrometer equatorially mounted in a dome. It was commissioned in
1985.

The site is situated close to sea level, near to the Indian Ocean.
The other buildings in the picture above are the relics of the old
Carnarvon satellite tracking station.

<a href="{{site.baseurl}}/assets/images/pages/carnarvon_big.gif"><img
style="float: left" alt="[Carnarvon site picture]"
src="{{site.baseurl}}/assets/images/pages/carnarvon.gif"></a>

<iframe width="280" height="243" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.uk/maps?f=q&amp;iwloc=&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=-24.86725,+113.70374&amp;aq=&amp;sll=-24.867252,113.703732&amp;sspn=0.001317,0.002411&amp;ie=UTF8&amp;t=h&amp;ll=-24.868216,113.703604&amp;spn=0.004731,0.006008&amp;z=16&amp;output=embed"></iframe>

## Narrabri, New South Wales, Australia

Narrabri was commisioned in 1992 - the instrument is equatorially
mounted and housed in a small dome. The station is situated within the
grounds of the ATNF's [Paul Wild
Observatory](http://www.narrabri.atnf.csiro.au).

<a href="{{site.baseurl}}/assets/images/pages/DQ-3.jpg"><img
style="float: left" alt="[Narrabri site picture]"
src="{{site.baseurl}}/assets/images/pages/DQ-3-small.jpg"></a>

<iframe width="280" height="204" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.uk/maps?f=q&amp;iwloc=&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=-30.309838,+149.565296&amp;aq=&amp;sll=-24.867252,113.703738&amp;sspn=0.001317,0.002411&amp;ie=UTF8&amp;t=h&amp;ll=-30.31169,149.56358&amp;spn=0.015116,0.023947&amp;z=14&amp;output=embed"></iframe>

<a href="{{site.baseurl}}/assets/images/pages/CG-21.jpg"><img
style="float: none" alt="[Narrabri site picture]"
src="{{site.baseurl}}/assets/images/pages/CG-21-small.jpg"></a>

<a href="{{site.baseurl}}/assets/images/pages/EB-17.jpg"><img
style="float: none" alt="[Narrabri site picture]"
src="{{site.baseurl}}/assets/images/pages/EB-17-small.jpg"></a>

<a href="{{site.baseurl}}/assets/images/pages/narrabri_atnf_big.gif"><img
style="float: none" alt="[Narrabri site picture]"
src="{{site.baseurl}}/assets/images/pages/narrabri_atnf.gif"></a>
