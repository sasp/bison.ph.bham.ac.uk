---
title: Primer
permalink: /background/primer
key: background-primer
---

## BiSON Primer Document

This document provides background material on the BiSON:

* A Primer on the collection, calibration and analysis of BiSON
data (elements being developed by the BiPSAL and solarFLAG
programmes).

* A summary on the scientific need for long, high-data-fill
coverage by the BiSON.

The BiSON primer is available in PDF format [here](/downloads/bisonprimer06.pdf).
