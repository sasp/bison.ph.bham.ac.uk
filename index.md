---
layout: article
title: Birmingham Solar Oscillations Network
key: page-about
article_header:
  type: cover
  image:
    src: /assets/images/pages/network_cover.png
---

See real time images and telemetry from the BiSON sites with [BiSON Live!]({{site.baseurl}}/live)
{: style="color:black; font-size: 150%; text-align: center;"}

[Download BiSON data here!]({{site.baseurl}}/opendata)
{: style="color:black; font-size: 150%; text-align: center;"}

The Birmingham Solar-Oscillations Network (BiSON) is operated by the
[Solar and Stellar Physics
Group](http://www.birmingham.ac.uk/research/activity/physics/astronomy/solar-and-stellar/index.aspx)
at the [University of Birmingham,
UK](https://www.birmingham.ac.uk/index.aspx). This world-wide network
of six remotely operated ground-based telescopes provides
round-the-clock monitoring of the globally coherent, core-penetrating
modes of oscillation of the Sun. BiSON is funded by the UK [Science
and Technology Facilities Council (STFC)](http://www.stfc.ac.uk/).
