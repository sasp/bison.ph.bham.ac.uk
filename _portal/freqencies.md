---
title: BiSON Frequencies
permalink: /portal/frequencies
key: portal-frequencies
---

The Birmingham Solar Oscillations Network (BiSON) provides
high-quality high-cadence observations from as far back in time as 1978.
Here we present the frequencies for the Sun-as-a-star global modes of
oscillation.

## 2014 - Low-frequency, low-degree solar p-mode properties.

The solar low-degree low-frequency modes of oscillation are of
particular interest as their frequencies can be measured with very
high precision and hence provide good constraints on seismic models.
Here we detect and characterize these valuable measures of the solar
interior from a 22 yr Birmingham Solar Oscillations Network data set.
We report mode frequencies, line widths, heights, amplitudes, and
rotational splitting, all with robust uncertainties. The new values of
frequency, rotational splitting, amplitude, and line width we provide
will help place new constraints on hydrostatic and rotational
structure, plus diagnostics of near-surface convection. Further to
this, by assuming simple power laws, we extrapolate mode properties to
lower frequencies. We demonstrate that the low-l low-frequency p modes
have a low signal-to-noise ratio and that this cannot be overcome
simply by continued observation. It will be necessary to observe the
Sun in novel ways to 'beat' the intrinsic granulation noise.

When making use of these data, please cite both
[10.1007/s11207-015-0810-0](http://dx.doi.org/10.1007/s11207-015-0810-0)
and [10.1093/mnras/stu080](http://dx.doi.org/10.1093/mnras/stu080) in
any publications.  Suggested BibTeX entries are below.

{% highlight bibtex %}
{% raw %}
@Article{s11207-015-0810-0,
 author = "Hale, S. J. and Howe, R. and Chaplin, W. J. and Davies, G. R. and Elsworth, Y. P.",
  title = "{{Performance of the Birmingham Solar-Oscillations Network (BiSON)}}",
journal = {\solphys},
   year = {2016},
  month = jan,
    day = {1},
 volume = {291},
 number = {1},
  pages = {1--28},
   issn = {1573-093X},
    doi = "10.1007/s11207-015-0810-0",
    url = "https://doi.org/10.1007/s11207-015-0810-0"
}
{% endraw %}
{% endhighlight %}

{% highlight bibtex %}
{% raw %}
@article{10.1093/mnras/stu080,
    author = {Davies, G. R. and Broomhall, A. M. and Chaplin, W. J. and Elsworth, Y. and Hale, S. J.},
    title = "{Low-frequency, low-degree solar p-mode properties from 22 years of Birmingham Solar Oscillations Network data}",
    journal = {Monthly Notices of the Royal Astronomical Society},
    volume = {439},
    number = {2},
    pages = {2025-2032},
    year = {2014},
    month = {02},
    issn = {0035-8711},
    doi = {10.1093/mnras/stu080},
    url = {https://doi.org/10.1093/mnras/stu080},
    eprint = {http://oup.prod.sis.lan/mnras/article-pdf/439/2/2025/18472646/stu080.pdf},
}
{% endraw %}
{% endhighlight %}

MD5: c89ac7efff269e4ab548803e08001980
{:.info}

[Download: davies2014.txt](/downloads/data/davies2014.txt){:.button.button--success.button--rounded}

{% highlight data %}
{% raw %}
         n  l     Freq   Error
         6  0   972.615  0.002 
         7  0  1117.993  0.004 
         7  1  1185.604  0.003 
         8  0  1263.198  0.005 
         8  1  1329.635  0.003 
         8  2  1394.689  0.005 
         9  0  1407.472  0.006 
         9  1  1472.839  0.006 
         9  2  1535.853  0.005
        10  0  1548.336  0.007
         9  3  1591.536  0.014 
        10  1  1612.724  0.006
        10  2  1674.538  0.008 
        11  0  1686.594  0.012 
        10  3  1729.088  0.022 
        11  1  1749.285  0.007 
        11  2  1810.308  0.009 
        12  0  1822.202  0.012 
        11  3  1865.280  0.016 
        12  1  1885.089  0.009 
        12  2  1945.824  0.013 
        13  0  1957.452  0.012 
{% endraw %}
{% endhighlight %}


## 2009 - Definitive Sun-as-a-star p-mode frequencies.

We present a list of 'best possible estimates' of low-degree
p-mode frequencies from 8640 days of observations made by the
Birmingham Solar-Oscillations Network (BiSON). This is the longest
stretch of helioseismic data ever used for this purpose, giving
exquisite precision in the estimated frequencies. Every effort has
been made in the analysis to ensure that the frequency estimates are
also accurate. In addition to presenting the raw best-fitting
frequencies from our 'peak-bagging' analysis, we also provide tables
of corrected frequencies pertinent to the quiet-Sun and an
intermediate level of solar activity.

When making use of these data, please cite both
[10.1007/s11207-015-0810-0](http://dx.doi.org/10.1007/s11207-015-0810-0)
and
[10.1111/j.1745-3933.2009.00672.x](http://dx.doi.org/10.1111/j.1745-3933.2009.00672.x)
in any publications.  Suggested BibTeX entries are below.

{% highlight bibtex %}
{% raw %}
@Article{s11207-015-0810-0,
 author = "Hale, S. J. and Howe, R. and Chaplin, W. J. and Davies, G. R. and Elsworth, Y. P.",
  title = "{{Performance of the Birmingham Solar-Oscillations Network (BiSON)}}",
journal = {\solphys},
   year = {2016},
  month = jan,
    day = {1},
 volume = {291},
 number = {1},
  pages = {1--28},
   issn = {1573-093X},
    doi = "10.1007/s11207-015-0810-0",
    url = "https://doi.org/10.1007/s11207-015-0810-0"
}
{% endraw %}
{% endhighlight %}

{% highlight bibtex %}
{% raw %}
@article{10.1111/j.1745-3933.2009.00672.x,
    author = {Broomhall, A.-M. and Chaplin, W. J. and Davies, G. R. and Elsworth, Y. and Fletcher, S. T. and Hale, S. J. and Miller, B. and New, R.},
    title = "{Definitive Sun-as-a-star p-mode frequencies: 23 years of BiSON observations}",
    journal = {Monthly Notices of the Royal Astronomical Society: Letters},
    volume = {396},
    number = {1},
    pages = {L100-L104},
    year = {2009},
    month = {06},
    issn = {1745-3925},
    doi = {10.1111/j.1745-3933.2009.00672.x},
    url = {https://doi.org/10.1111/j.1745-3933.2009.00672.x},
    eprint = {http://oup.prod.sis.lan/mnrasl/article-pdf/396/1/L100/3024124/396-1-L100.pdf},
}
{% endraw %}
{% endhighlight %}

MD5: d329a8da84b66b4a3b2716eb2fe906f6
{:.info}

[Download: broomhall2009.txt](/downloads/data/broomhall2009.txt){:.button.button--success.button--rounded}

These are corrected frequencies pertinent to the quiet Sun.

{% highlight data %}
{% raw %}
         n  l     Freq   Error
         6  0   972.613  0.002
         7  1  1185.592  0.004
         8  0  1263.162  0.012 
         8  1  1329.629  0.004 
         8  2  1394.680  0.011
         9  0  1407.481  0.012 
         9  1  1472.835  0.007 
         9  2  1535.859  0.011
        10  0  1548.333  0.007 
        10  1  1612.720  0.007 
        10  2  1674.534  0.008 
        10  3  1729.085  0.016
        11  0  1686.597  0.011 
        11  1  1749.279  0.007 
        11  2  1810.304  0.009 
        11  3  1865.293  0.019
        12  0  1822.196  0.011 
        12  1  1885.081  0.009 
        12  2  1945.797  0.013 
        12  3  2001.243  0.017
        13  0  1957.421  0.012 
        13  1  2020.793  0.010 
        13  2  2082.102  0.015 
        13  3  2137.788  0.019
        14  0  2093.518  0.013 
        14  1  2156.784  0.014 
        14  2  2217.656  0.018 
        14  3  2273.516  0.026
        15  0  2228.749  0.014 
        15  1  2291.993  0.015 
        15  2  2352.222  0.017
        15  3  2407.643  0.025
        16  0  2362.788  0.016 
        16  1  2425.595  0.015 
        16  2  2485.873  0.019 
        16  3  2541.671  0.023
        17  0  2496.180  0.017 
        17  1  2559.162  0.015 
        17  2  2619.668  0.018 
        17  3  2676.149  0.023
        18  0  2629.668  0.015 
        18  1  2693.347  0.014 
        18  2  2754.439  0.017 
        18  3  2811.318  0.021
        19  0  2764.142  0.015 
        19  1  2828.150  0.014 
        19  2  2889.578  0.018 
        19  3  2946.935  0.021
        20  0  2899.022  0.013 
        20  1  2963.322  0.014 
        20  2  3024.689  0.018 
        20  3  3082.273  0.025
        21  0  3033.754  0.014 
        21  1  3098.140  0.015 
        21  2  3159.821  0.020 
        21  3  3217.683  0.028
        22  0  3168.618  0.017 
        22  1  3233.139  0.018 
        22  2  3295.063  0.025 
        22  3  3353.335  0.041
        23  0  3303.520  0.021 
        23  1  3368.469  0.023 
        23  2  3430.748  0.034 
        23  3  3489.409  0.053
        24  0  3438.992  0.030 
        24  1  3504.129  0.030 
        24  2  3566.949  0.044 
        24  3  3626.078  0.075
        25  0  3574.893  0.048 
        25  1  3640.475  0.038 
        25  2  3703.044  0.067 
        25  3  3762.530  0.105
        26  0  3710.717  0.088 
        26  1  3777.067  0.052 
        26  2  3839.717  0.144
        27  0  3846.993  0.177 
        27  1  3913.570  0.068 
        27  2  3976.930  0.298
        28  0  3984.214  0.323
{% endraw %}
{% endhighlight %}
