---
title: BiSON Time Series
permalink: /portal/timeseries
key: portal-timeseries
---

The Birmingham Solar Oscillations Network (BiSON) provides
high-quality high-cadence observations from as far back in time as 1978.
However, 1985 is the earliest period for which at least three sites
were observing regularly.

These data are calibrated from the raw observations into radial
velocity and the quality of the calibration has a large impact on the
signal-to-noise ratio of the final time series.  For details on this
procedure please see [10.1093/mnras/stu803](http://dx.doi.org/10.1093/mnras/stu803).

If you would like a specific time period of data or have a special
processing request, please contact us for a bespoke solution.  When
making use of these data, please cite both
[10.1007/s11207-015-0810-0](http://dx.doi.org/10.1007/s11207-015-0810-0) and
[10.1093/mnras/stu803](http://dx.doi.org/10.1093/mnras/stu803) in any publications.
Suggested BibTeX entries are below.


{% highlight bibtex %}
{% raw %}
@Article{s11207-015-0810-0,
 author = "Hale, S. J. and Howe, R. and Chaplin, W. J. and Davies, G. R. and Elsworth, Y. P.",
  title = "{{Performance of the Birmingham Solar-Oscillations Network (BiSON)}}",
journal = {\solphys},
   year = {2016},
  month = jan,
    day = {1},
 volume = {291},
 number = {1},
  pages = {1--28},
   issn = {1573-093X},
    doi = "10.1007/s11207-015-0810-0",
    url = "https://doi.org/10.1007/s11207-015-0810-0"
}
{% endraw %}
{% endhighlight %}


{% highlight bibtex %}
{% raw %}
@article{doi:10.1093/mnras/stu803,
author = {Davies, G. R. and Chaplin, W. J. and Elsworth, Y. P. and Hale, S. J.},
title = {BiSON data preparation: a correction for differential extinction and the weighted averaging of contemporaneous data},
journal = {Monthly Notices of the Royal Astronomical Society},
volume = {441},
number = {4},
pages = {3009-3017},
year = {2014},
doi = {10.1093/mnras/stu803},
URL = {http://dx.doi.org/10.1093/mnras/stu803},
}
{% endraw %}
{% endhighlight %}


## All sites - 1976/01 to 2025/02 - Optimised for Fill

{% highlight fits %}
{% raw %}
        SIMPLE  =                    T / Written by IDL:  Tue Feb  4 10:24:01 2025
        BITPIX  =                  -64 /Real*8 (double precision)
        NAXIS   =                    2 /
        NAXIS1  =                    2 /
        NAXIS2  =             38726638 /
        D-START =        2442778.50000 /Data start (Julian)
        D-END   =        2460707.50000 /Data end (Julian)
        STATIONS= 'na ca cb su iz la mo ha' /Stations Used
        METHOD  = 'waverage'           /Overlap Method
        CADENCE = '40      '           /Timeseries Cadence
        N-THRES =                  150 /Noise Rejection Threshold
        GAPFILL =                    1 /GapFill
        FILL    =             0.559483 /Duty Cycle
        COMMENT Generated by BiSON
        COMMENT $Id: svnid.s 194 2011-11-15 10:13:23Z hale $
        END
{% endraw %}
{% endhighlight %}

SHA256: e4e176abe6f6930197155fbd44f20e4d99e7151c4b033f10c6bc8137b9c6d03e
{:.info}
MD5: 3a10acc92e9fb91191788934f0bc3bd7
{:.info}

[Download: allsites-alldata-waverage-fill.fits.gz](/downloads/data/allsites-alldata-waverage-fill.fits.gz){:.button.button--success.button--rounded}


## All sites - 1985/01 to 2024/01 - Optimised for Quality

{% highlight fits %}
{% raw %}
        SIMPLE  =                    T / Written by IDL:  Mon Jan  8 21:20:32 2024
        BITPIX  =                  -64 /Real*8 (double precision)
        NAXIS   =                    2 /
        NAXIS1  =                    2 /
        NAXIS2  =             30767039 /
        D-START =        2446066.50000 /Data start (Julian)
        D-END   =        2460310.50000 /Data end (Julian)
        STATIONS= 'na ca cb su iz la mo ha' /Stations Used
        METHOD  = 'waverage'           /Overlap Method
        CADENCE = '40      '           /Timeseries Cadence
        N-THRES =                   80 /Noise Rejection Threshold
        GAPFILL =                    1 /GapFill
        FILL    =             0.637600 /Duty Cycle
        COMMENT Generated by BiSON
        COMMENT $Id$
        END
{% endraw %}
{% endhighlight %}

SHA256: 979a466b231a0a5a4931ebc711d706de1cea3fead2030a006803792db76c5401
{:.info}
MD5: e8e65fe021b94575163e795d437b2467
{:.info}

[Download: allsites-waverage-quality.fits.gz](/downloads/data/allsites-waverage-quality.fits.gz){:.button.button--success.button--rounded}


## All sites - 1995 to 2014 - Performance Check

{% highlight fits %}
{% raw %}
        SIMPLE  =                    T / Written by IDL:  Wed May 27 11:08:33 2015      
        BITPIX  =                  -64 /Real*8 (double precision)                       
        NAXIS   =                    2 /                                                
        NAXIS1  =                    2 /                                                
        NAXIS2  =             15778799 /                                                
        D-START =        2449718.50000 /Data start (Julian)                             
        D-END   =        2457023.50000 /Data end (Julian)                               
        STATIONS= 'na ca cb su iz la mo' /Stations Used                                 
        METHOD  = 'waverage'           /Overlap Method                                  
        CADENCE = '40      '           /Timeseries Cadence                              
        N-THRES =                  100 /Noise Rejection Threshold                       
        FILL    =             0.783800 /Duty Cycle                                      
        COMMENT Generated by BiSON
        COMMENT $Id$
        END
{% endraw %}
{% endhighlight %}

In addition to the above citations, if using this dataset please also
cite the [dataset itself](https://edata.bham.ac.uk/59/) in any
publications.  A suggested BibTeX entry is below.

{% highlight bibtex %}
{% raw %}
@misc{edata59,
      title = {{BiSON - All Sites - 1995 to 2014 - Performance Check}},
     author = {Steven J. Hale},
  publisher = {Birmingham Solar Oscillations Network},
       year = {2015},
      month = may,
        doi = "10.25500/eData.bham.00000059",
        url = {https://edata.bham.ac.uk/59/}
}
{% endraw %}
{% endhighlight %}

SHA256: 9871278333ed07bc54b51532c3dc8eb54d4c5da1981e6df75cbe68485f761c43
{:.info}
MD5: a8e28b5532d449202a5e55b6452812da
{:.info}

[Download: bison-allsites-1995-2014.fits.gz](https://edata.bham.ac.uk/59/){:.button.button--success.button--rounded}
