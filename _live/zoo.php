<?php

function refreshPaddocks($tmppath, $address) {
    $options = "-q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no";
    $zookey = "-i /var/www/.ssh/zoostatus";
    exec("rm $tmppath/*.lck > /dev/null 2> /dev/null");
    if(strpos($address, 'izana') !== false) exec("/home/stations/izana/bin/firewall");
    exec("scp $options $zookey $address: $tmppath");
}

function displayAnimal($filespec) {

    if (file_exists($filespec)) {
        $animal = file($filespec);

        $animal = str_replace("@blue{",    "<span style=\"color: blue\">", $animal);
        $animal = str_replace("@magenta{", "<span style=\"color: magenta\">", $animal);
        $animal = str_replace("@green{",   "<span style=\"color: green\">", $animal);
        $animal = str_replace("@yellow{",  "<span style=\"color: orange\">", $animal);
        $animal = str_replace("@red{",     "<span style=\"color: red\">", $animal);
        $animal = str_replace("}", "</span>", $animal);

        echo "<div class=\"zoostatus\">\n";
        echo "<pre>\n";
        foreach ($animal as $line) {
            if ($line != "") echo "$line";
        }
        echo "</pre>\n";
        echo "</div>\n";

    } else {
        //echo "<p>No information available.</p>\n";
    }
}

function displayZoo($tmppath, $address) {

    $refreshTime = 60;

    if (!file_exists($tmppath)) {
        mkdir($tmppath);
    }

    $refresh = false;
    $filename = "$tmppath/eagle.lck";
    if (file_exists($filename)) {
        $age = time() - filemtime($filename);
        if ($age < $refreshTime) {
            $delay = $refreshTime - $age;
            echo "<p><span style=\"color: red\">Time to next refresh: " . $delay . " seconds.</span></p>\n";
        } else {
            $refresh = true;
        }
    } else {
        $refresh = true;
    }

    if ($refresh) refreshPaddocks($tmppath, $address);

    echo "<br /><hr /><br />\n";
    echo "<h2>Dome</h2>\n";
    ?>
    <?php
    displayAnimal("$tmppath/elephant.lck");
    echo "<br /><hr /><br />\n";
    echo "<h2>Mount</h2>\n";
    displayAnimal("$tmppath/giraffe.lck");
    displayAnimal("$tmppath/antelope.lck");
    echo "<br /><hr /><br />\n";
    echo "<h2>Detectors</h2>\n";
    displayAnimal("$tmppath/tiger.lck");
    echo "<br /><hr /><br />\n";
    echo "<h2>Temperature Controllers</h2>\n";
    displayAnimal("$tmppath/iguana.lck");
    displayAnimal("$tmppath/goanna.lck");
    echo "<br /><hr /><br />\n";
    echo "<h2>Temperature Monitors</h2>\n";
    displayAnimal("$tmppath/lizard.lck");
    echo "<br /><hr /><br />\n";
    echo "<h2>Watchdogs</h2>\n";
    displayAnimal("$tmppath/eagle.lck");
    displayAnimal("$tmppath/turkey.lck");

}

?>
