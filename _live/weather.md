---
title: BiSON Weather Watcher
permalink: /live/weather
key: live-weather
---

Here you can obtain current images of cloud cover over our stations,
along with a worldwide image showing current weather patterns. The
cloud maps are updated every 3 hours, while the terminator (the line
separating day from night) is updated hourly. Be sure to hit refresh
in your web browser to obtain the most up-to-date images.

The images and cloud maps are produced using [xplanet](http://xplanet.sourceforge.net/).

## Global Conditions

![Global Conditions](/assets/images/maps/allearth.jpg "Global Conditions")

## Mount Wilson, California

![Mount Wilson](/assets/images/maps/wilson.jpg "Mount Wilson")

## Las Campanas, Chile

![Las Campanas](/assets/images/maps/campanas.jpg "Las Campanas")

## Izana, Tenerife

![Izana](/assets/images/maps/izana.jpg "Izana")

## Birmingham, UK

![Birmingham](/assets/images/maps/birmingham.jpg "Birmingham")

## Sutherland, South Africa

![Sutherland](/assets/images/maps/sutherland.jpg "Sutherland")

## Carnarvon, Western Australia

![Carnarvon](/assets/images/maps/carnarvon.jpg "Carnarvon")

## Narrabri, New South Wales, Australia

![Carnarvon](/assets/images/maps/narrabri.jpg "Carnarvon")
